import 'package:flutter/material.dart';


void main() {
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    Widget nameSection = Container(
      padding: const EdgeInsets.all(32),
      child: Center(
        child: Text(
          'Jason Luu',
          style: TextStyle(
            fontSize:40,
            fontWeight: FontWeight.bold
          ),
        )
      )
    );
    Color color = Theme.of(context).primaryColor;
    Widget shiftSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.access_time, 'My Shift'),
          _buildButtonColumn(color, Icons.calendar_view_day, 'Open Shift'),
          _buildButtonColumn(color, Icons.calendar_today, 'All Shift'),
        ],
      )
    );
    Widget aboutSection = Container(
      padding: EdgeInsets.all(32),
      child: Text(
        'About me:\n'
        '\t Hired since: 2019\n'
        '\t Position: Front Staff\n'
        '\t Part/Full Time: Part-time\n'
        '\t Occupation: Student\n'
        '\t Hobbies: Coding, Walking, Swimming, Basketball',
        softWrap: true
      )
    );

    return MaterialApp(
      title: 'Layout Homework',
      home: Scaffold(
        appBar: AppBar(
          title: Text('My Profile'),
          leading: Icon(
            Icons.menu,
          ),
        ),
        body:ListView(
          children: [
            Image.asset(
              'images/Picture1.png',
              fit: BoxFit.cover,
            ),
            nameSection,
            shiftSection,
            aboutSection
          ],
        ),

      ),
    );
  }
  Column _buildButtonColumn(Color color, IconData icon, String label){
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children:[
        Container(
          margin: const EdgeInsets.only(bottom: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color:color,
            ),
          ),
        ),
        Icon(
            icon,
            color:color
        ),

      ],
    );
  }

}//MyApp